CPMAddPackage(
    NAME fmt
    GIT_TAG 9.1.0
    GITHUB_REPOSITORY fmtlib/fmt
)

CPMAddPackage(
    NAME nlohmann_json
    URL https://github.com/nlohmann/json/archive/refs/tags/v3.11.2.zip
    VERSION 3.11.2
)

CPMAddPackage("gh:gabime/spdlog@1.11.0")